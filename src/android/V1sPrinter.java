package cordova.plugin.v1sprinter;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

// import cordova.plugin.v1sprinter.BluetoothUtil;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.text.LoginFilter;
import android.util.Log;
import android.widget.Toast;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

import woyou.aidlservice.jiuiv5.ICallback;
import woyou.aidlservice.jiuiv5.IWoyouService;

/**
 * This class echoes a string called from JavaScript.
 */
public class V1sPrinter extends CordovaPlugin {
    /* AIDL */
    private IWoyouService woyouService;

    public void sendRawData(byte[] data) {
        if (woyouService == null) {
            Log.i("kaltin", "The service has been disconnected");
            return;
        }

        // try {
            woyouService.sendRAWData(data, null);
        // } catch (RemoteException e) {
        //     e.printStackTrace();
        // }
    }

    @Override
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
        if (action.equals("printText")) {
            this.printText(args, callbackContext);
            return true;
        }
        return false;
    }

    private void printText(JSONArray args, CallbackContext callbackContext) {
      if (args != null) {
        try {
          String content = args.getJSONObject(0).getString("content");
          float size = Float.parseFloat(args.getJSONObject(0).getString("size"));
          boolean isBold = Boolean.parseBoolean(args.getJSONObject(0).getString("isBold"));
          boolean isUnderLine = Boolean.parseBoolean(args.getJSONObject(0).getString("isUnderLine"));

          String strContent = content + " " + size + " " + isBold + " " + isUnderLine;
          byte[] bContent = strContent.getBytes();
          // this.sendData(bContent);
          this.sendRawData(bContent);

          callbackContext.success(content + " " + size + " " + isBold + " " + isUnderLine);
        } catch(Exception ex) {
          callbackContext.error("Something went wrong. " + ex);
        }
      } else {
        callbackContext.error("Expected one non-empty array argument.");
      }
    }

    /* BLUETOOTH */
    private static final UUID PRINTER_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");

    private static final String Innerprinter_Address = "00:11:22:33:44:55";

    private static BluetoothSocket bluetoothSocket;

    private static BluetoothAdapter getBTAdapter() {
        return BluetoothAdapter.getDefaultAdapter();
    }

    private static BluetoothDevice getDevice(BluetoothAdapter bluetoothAdapter) {
        BluetoothDevice innerprinter_device = null;
        Set<BluetoothDevice> devices = bluetoothAdapter.getBondedDevices();
        for (BluetoothDevice device : devices) {
            if (device.getAddress().equals(Innerprinter_Address)) {
                innerprinter_device = device;
                break;
            }
        }
        return innerprinter_device;
    }

    private static BluetoothSocket getSocket(BluetoothDevice device) throws IOException {
        BluetoothSocket socket;
        socket = device.createRfcommSocketToServiceRecord(PRINTER_UUID);
        socket.connect();
        return  socket;
    }

    /**
     * Connect Bluetooth
     *
     * @param context context
     * @return true success false failed
     */
    public static boolean connectBlueTooth(Context context) {
        if (bluetoothSocket == null) {
            if (getBTAdapter() == null) {
                Toast.makeText(context, "Bluetooth device is unavailable", Toast.LENGTH_SHORT).show();
                return false;
            }
            if (!getBTAdapter().isEnabled()) {
                Toast.makeText(context, "Bluetooth device not detected. Please open Bluetooth!", Toast.LENGTH_SHORT).show();
                return false;
            }
            BluetoothDevice device;
            if ((device = getDevice(getBTAdapter())) == null) {
                Toast.makeText(context, "InnterPrinter Not Found !", Toast.LENGTH_SHORT).show();
                return false;
            }

            try {
                bluetoothSocket = getSocket(device);
            } catch (IOException e) {
                Toast.makeText(context, "Bluetooth connection failed!", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    /**
     * Disconnect Bluetooth
     */
    public static void disconnectBlueTooth(Context context) {
        if (bluetoothSocket != null) {
            try {
                OutputStream out = bluetoothSocket.getOutputStream();
                out.close();
                bluetoothSocket.close();
                bluetoothSocket = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Send data to Bluetooth
     *
     * @param bytes
     * @throws IOException
     */
    public static void sendData(byte[] bytes) {
        if (bluetoothSocket != null) {
            OutputStream out = null;
            try {
                out = bluetoothSocket.getOutputStream();
                out.write(bytes, 0, bytes.length);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Log.i("kaltin", "bluetoothSocketttt null");
        }
    }
}
