var exec = require('cordova/exec');

module.exports.printText = function (arg0, success, error) {
    exec(success, error, 'V1sPrinter', 'printText', [arg0]);
}
